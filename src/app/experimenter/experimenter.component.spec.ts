import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperimenterComponent } from './experimenter.component';

describe('ExperimenterComponent', () => {
  let component: ExperimenterComponent;
  let fixture: ComponentFixture<ExperimenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperimenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperimenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
