import {Component, OnInit} from '@angular/core';
import { MaterialBasis } from "../material/material-basis";
import {PresenterBasis} from '../presenter/presenter-basis';

@Component({
  selector: 'app-experimenter',
  templateUrl: './experimenter.component.html',
  styleUrls: ['./experimenter.component.css']
})
export class ExperimenterComponent implements OnInit {
  // later, maybe replace these with an integrated real solution
  sampleMaterial: MaterialBasis;
  samplePresenter: PresenterBasis;

  constructor() {
    // later, probably replace this with an integrated real solution
    this.sampleMaterial = new MaterialBasis(0);
    this.sampleMaterial.content = "This is sample text material.";
    this.samplePresenter = new PresenterBasis(0, "Sample", this.sampleMaterial);
  }

  ngOnInit(): void {
  }

}
