import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComposableComponent } from './composable.component';

describe('ComposableComponent', () => {
  let component: ComposableComponent;
  let fixture: ComponentFixture<ComposableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComposableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComposableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
