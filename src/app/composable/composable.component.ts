import {Component, OnInit} from '@angular/core';
import {ComposableBasis} from './composable-basis';
import {DragAndDropService} from '../drag-and-drop/drag-and-drop.service';

/* a ComposableComponent represents any ComposableBasis subclass,
   but it primarily serves as a location to host drag-and-drop
   capabilities in one place for all of its subclasses */

@Component({
  selector: 'app-composable',
  template: '',
  styles: [''],
  providers: [DragAndDropService]
})
export class ComposableComponent implements OnInit {
  basis: ComposableBasis;
  dnd: DragAndDropService;

  // elements are the type names of ComposableBasis subclasses
  droppableTypes: string[] = [];

  constructor(dnd: DragAndDropService) {
    this.dnd = dnd;
  }

  ngOnInit(): void {
  }

  whenDragged(e: DragEvent) {
    // preventing this event from being raised on all containers
    e.stopPropagation();

    // using the Singleton DND service to actually perform object handling
    this.dnd.setDraggees([this.basis]);

    // retaining the ID for the JS DND system for reference
    let id = (<Element> e.target).id;
    e.dataTransfer.setData('text/plain', id);
  }

  whenDraggedOver(e: DragEvent) {
    e.preventDefault();

    let draggees = this.dnd.getDraggees();
    let draggee = draggees[0];

    let isDroppable = false;

    for (let type of this.droppableTypes) {
      let declared = draggee.declaredType;

      if (declared == type) {
        isDroppable = true;
        break;
      }
    }

    if (isDroppable) {
      e.dataTransfer.dropEffect = 'copy';
    }
    else {
      e.dataTransfer.dropEffect = 'none';
    }
  }

  whenDropped(e: DragEvent) {
    /* a Template Method */

    // both of these always
    e.preventDefault();
    e.stopPropagation();

    // the pattern: actual use of draggee
    this.dropHandler();
  }

  dropHandler(): void {
    /* no implementation: base for overrides only */
  }
}
