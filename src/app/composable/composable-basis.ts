/**/

/* ComposableBasis is the stored definition of any of
   its subclasses, existing in a graph or Composite. */

export class ComposableBasis {
  name: string;
  id: number;

  // hack; later, replace this with real type usage
  declaredType: string;

  // recursive structure: storage IDs of child instances
  children: number[] = [];

  constructor(id: number, name: string = null) {
    this.id = id;
    this.name = name;
  }

  /* must be overridden by derived classes */
  clone() {
    let copy = new ComposableBasis(this.id, this.name);
    return copy;
  }
}
