import {Component, OnInit, ViewChild} from '@angular/core';
import {Lexeme} from '../lexeme';
import {Expression} from '../expression';
import {SpinnerComponent} from '../spinner/spinner.component';
import { FrameComponent } from "../frame/frame.component";

/* later, remove some and maybe all of these, as services do the trick instead */

import {Operation} from '../lexemes/operation';
import {Operand} from '../lexemes/operand';
import {Notation} from '../lexemes/notation';

import {Addition} from '../lexemes/operations/addition';
import {Subtraction} from '../lexemes/operations/subtraction';
import {Multiplication} from '../lexemes/operations/multiplication';
import {Division} from '../lexemes/operations/division';

import {SingleDigits} from '../lexemes/operands/single-digits';
import {DoubleDigits} from '../lexemes/operands/double-digits';
import {TripleDigits} from '../lexemes/operands/triple-digits';

import {Infix} from '../lexemes/notations/infix';
import {Polish} from '../lexemes/notations/polish';


@Component({
  selector: 'app-teacher-mode',
  templateUrl: './teacher-mode.component.html',
  styleUrls: ['./teacher-mode.component.css']
})
export class TeacherModeComponent implements OnInit {
  operations: Operation[] = [];
  operands: Operand[] = [];
  notations: Notation[] = [];

  /* types work as desired, so these
     can hold objects of subtypes */
  lexemes: Lexeme[];
  dragged: Lexeme;

  expression: Expression = new Expression();

  expressions: Expression[] = [];

  @ViewChild(SpinnerComponent, {static: true}) spinner;

  constructor() {
    /* later, get options from the service and
       then have that get them from a server */
    this.cruftBuildLexemeLists();
  }

  ngOnInit(): void {
  }

  whenDragStarted(e) {
    let id = e.target.id;
    let lexeme = this.lexemes.find(x => x.id == id);
    this.dragged = lexeme;

    // although these aren't needed for Angular, some browsers don't
    // support drag and drop without the first of these, at least
    e.dataTransfer.setData('text/plain', id);
    e.dropEffect = 'copy';
  }

  whenDraggedOver(e: MouseEvent) {
    e.preventDefault();
  }

  whenDropped(e: DragEvent) {
    e.preventDefault();
    e.stopPropagation();

    let target = e.target as Element;
    let id = target.id;
    let at = this.expression.lexemes.findIndex(x => x.id == id);

    this.expression.addLexemeClone(this.dragged, at);
  }

  whenDroppedOnFrame(onto : number) {
    let notation = this.expression.notation;
    let at = notation.correlations.get(onto);
    this.expression.addLexemeClone(this.dragged, at);
  }

  addToExpressions() {
    // making and adding copies, some of whose
    // members are identical, like Flyweights
    for (let at = 0; at < this.spinner.quantity; at++) {
      let clone = this.expression.clone();
      this.expressions.push(clone);
    }
  }

  resetBuilder() {
    /* the current notation is preserved, since that seems desirable */

    let notation = this.expression.notation;
    this.expression = new Expression();
    this.expression.notation = notation;
  }

  /* later, drop this in favor of service-provided lexemes */
  cruftBuildLexemeLists() {
    this.operations.push(
      new Addition(), new Subtraction(),
      new Multiplication(), new Division()
    );

    this.operands.push(
      new SingleDigits(),
      new DoubleDigits(),
      new TripleDigits()
    );

    this.notations.push(
      new Infix(),
      new Polish()
    );

    this.lexemes = [];
    this.lexemes = this.lexemes.concat(
      this.operations,
      this.operands,
      this.notations
    );
  }
}
