import {TestBed} from '@angular/core/testing';

import {DragAndDropService} from './drag-and-drop.service';
import {ComposableBasis} from '../composable/composable-basis';

describe('DragAndDropService', () => {
  let service: DragAndDropService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DragAndDropService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be inited with an empty .draggees', () => {
    let draggees = service.getDraggees();
    expect(draggees).toBeTruthy();
    expect(draggees.length).toEqual(0);
  });

  it('should have all-new draggees when .draggees is set', () => {
    /* arrange */
    let draggableOne = new ComposableBasis(1);
    let draggableTwo = new ComposableBasis(2);
    let draggables = [draggableOne, draggableTwo];

    /* pre-test */
    let draggees = service.getDraggees();
    expect(draggees.length).toEqual(0);

    /* act */
    service.setDraggees(draggables);

    /* assemble */
    // weirdly, the array acts like a value, not a reference
    draggees = service.getDraggees();

    /* assert */
    expect(draggees.length).toEqual(draggables.length);

    expect(draggees[0]).toBe(draggableOne);
    expect(draggees[1]).toBe(draggableTwo);
  });

  it('should have an empty .draggees when non-empty .draggees is cleared', () => {
    /* arrange */
    let draggableOne = new ComposableBasis(1);
    let draggableTwo = new ComposableBasis(2);
    let draggables = [draggableOne, draggableTwo];

    service.setDraggees(draggables);

    /* pre-test */
    let draggees = service.getDraggees();
    expect(draggees.length).toEqual(2);

    /* act */
    service.clearDraggees();

    /* assert */
    // must be set again; possibly arrays returned are value types
    draggees = service.getDraggees();
    expect(draggees.length).toEqual(0);
  });

  it('should return the targeted draggee when that draggee\'s ID is provided', () => {
    /* arrange */
    let draggableOne = new ComposableBasis(1);
    let draggableTwo = new ComposableBasis(2);
    let draggableThree = new ComposableBasis(3);
    let draggables = [draggableThree, draggableOne, draggableTwo];

    service.setDraggees(draggables);

    /* act */
    let actual = service.getDraggeeById('2');

    /* assert */
    expect(actual).toBe(draggableTwo);
  });

  it('should return null when the ID provided does not match any draggee', () => {
    /* arrange */
    let draggableOne = new ComposableBasis(1);
    let draggableThree = new ComposableBasis(3);
    let draggables = [draggableThree, draggableOne];

    service.setDraggees(draggables);

    /* act */
    let actual = service.getDraggeeById('2');

    /* assert */
    expect(actual).toBeNull();
  });
});
