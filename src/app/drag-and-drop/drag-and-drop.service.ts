import {Injectable} from '@angular/core';
import {ComposableBasis} from "../composable/composable-basis";

@Injectable({
  providedIn: 'root'
})
export class DragAndDropService {
  /* Provides a central location for composable items (only)
  *  that are being dragged and dropped, with operations
  *  to set and get all of them, or to get one by ID,
  *  since IDs can easily be used in drag and drop event handlers.
  *  Arbitrary mutation of the item collection is not supported. */

  private draggees: ComposableBasis[];

  constructor() /* passed */ {
    this.draggees = [];
  }

  setDraggees(draggees : ComposableBasis[]) /* passed */ {
    this.draggees = draggees;
  }

  clearDraggees() /* passed */ {
    this.draggees = [];
  }

  getDraggees() /* verified */ {
    return this.draggees;
  }

  getDraggeeById(id: string) : ComposableBasis /* passed */ {
    // HTML IDs are strings, but ComposableBasis IDs are (integer) numbers
    let idAsInt = Number(id);

    // actually finding, if present
    let draggee = this.draggees.find(x => x.id == idAsInt);

    // converting undefined (when not found) to null
    if (!draggee) {
      draggee = null;
    }

    // to caller
    return draggee;
  }
}
