
import {Lexeme} from "../lexeme";

export abstract class Operation implements Lexeme {
  abstract id: string;
  abstract name: string;
  abstract terms: number;

  abstract operate(...operands: [number]): number;
}
