
import { Lexeme } from "../lexeme";

export abstract class Operand implements Lexeme {
  abstract id: string;
  abstract name: string;

  abstract get value() : number;
}
