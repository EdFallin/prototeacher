
import { Operand } from "../operand";

export class DoubleDigits extends Operand {
  id: string = "DoubleDigits";
  name: string = "23";

  /* later, random 2-digit numbers */
  get value(): number { return 23; }
}
