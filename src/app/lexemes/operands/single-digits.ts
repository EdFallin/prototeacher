
import { Operand } from "../operand";

export class SingleDigits extends Operand {
  id: string = "SingleDigits";
  name: string = "1";

  /* later, random single-digit numbers */
  get value(): number { return 1; }
}
