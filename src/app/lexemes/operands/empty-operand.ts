/**/

import {Operand} from "../operand";

export class EmptyOperand extends Operand {
  id: string = "EmptyOperand"
  name: string = "[0]";

  get value(): number {
    return Number.NaN;
  }
}
