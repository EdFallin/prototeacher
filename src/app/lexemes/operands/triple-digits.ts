import {Operand} from '../operand';

export class TripleDigits extends Operand {
  id: string = 'TripleDigits';
  name: string = '456';

  /* later, random 3-digit numbers */
  get value(): number {
    return 456;
  }
}
