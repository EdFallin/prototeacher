import {Lexeme} from '../../lexeme';
import {Notation} from '../notation';
import {EmptyOperation} from '../operations/empty-operation';
import {EmptyOperand} from '../operands/empty-operand';

export class Polish extends Notation {
  id: string = 'Polish';
  name: string = '⊕xy';

  constructor() {
    super();

    // correlating polish indices to infix indixes
    this.correlations = new Map([[0, 1], [1, 0], [2, 2]]);
  }
}
