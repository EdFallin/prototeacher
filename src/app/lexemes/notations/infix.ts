import {Lexeme} from '../../lexeme';
import {Notation} from '../notation';
import {Operand} from '../operand';
import {EmptyOperation} from '../operations/empty-operation';
import {EmptyOperand} from '../operands/empty-operand';

export class Infix extends Notation {
  id: string = 'Infix';
  name: string = 'x⊕y';

  constructor() {
    super();

    // direct correlation because infix notation in used in Expression
    this.correlations = new Map([[0, 0], [1, 1], [2, 2]]);
  }
}
