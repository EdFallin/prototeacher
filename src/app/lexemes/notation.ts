import {Lexeme} from '../lexeme';

export class Notation implements Lexeme {
  id: string;
  name: string;

  correlations : Map<number, number> = new Map();
}
