/**/

import {Operation} from '../operation';

export class EmptyOperation extends Operation {
  id: string = "EmptyOperation";
  name: string = "⊕";
  terms: number = 2;

  operate(...operands: [number]): number {
    return Number.NaN;
  }
}
