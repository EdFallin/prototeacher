import {Operation} from '../operation';

export class Subtraction extends Operation {
  id: string = 'Subtraction';
  name: string = '−';
  terms: number = 2;

  operate(...operands: [number]): number {
    return 0;
  }
}
