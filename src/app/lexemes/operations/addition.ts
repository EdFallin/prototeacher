import {Operation} from '../operation';

export class Addition extends Operation {
  id: string = 'Addition';
  name: string = '+';
  terms: number = 2;

  operate(...operands: [number]): number {
    return 0;
  }
}
