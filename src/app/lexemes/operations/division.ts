import {Operation} from '../operation';

export class Division extends Operation {
  id: string = 'Division';
  name: string = '÷';
  terms: number = 2;

  operate(...operands: [number]): number {
    return 0;
  }
}
