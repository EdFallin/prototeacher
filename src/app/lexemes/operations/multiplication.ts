import {Operation} from '../operation';

export class Multiplication extends Operation {
  id: string = 'Multiplication';
  name: string = '×';
  terms: number = 2;

  operate(...operands: [number]): number {
    return 0;
  }
}
