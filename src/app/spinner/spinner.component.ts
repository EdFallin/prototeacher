import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {
  /* region definitions */

  editKeys: String[] = ['Backspace', 'Delete', 'ArrowLeft', 'ArrowRight', 'Shift'];

  /* endregion definitions */


  /* region public properties */

  public quantity: number = 1;

  /* endregion public properties */


  /* region construction */

  constructor() {
    /* no operations */
  }

  ngOnInit(): void {
  }

  /* endregion construction */


  /* region event handling */

  digitsOnly(e: KeyboardEvent) {
    let digit = Number(e.key);

    if (digit) {
      return;
    }

    if (this.editKeys.includes(e.key)) {
      return;
    }

    if (e.key == 'Backspace' || e.key == 'Delete') {
      return;
    }

    e.preventDefault();
  }

  incrementNumber() {
    this.restartIfNaN();
    this.quantity++;
  }

  decrementNumber() {
    this.restartIfNaN();

    // quantity can onlh be greater than 0
    this.quantity = Math.max(0, this.quantity - 1);
  }

  /* endregion event handling */


  /* region state */

  decrementorClass() {
    if (this.quantity == 0) {
      return 'spinner-button down disabled';
    }

    return 'spinner-button down';
  }

  /* endregion state */


  /* region dependencies */

  restartIfNaN() {
    // zero here, not one, because ++ or -- happens next, so zero is
    // more intuitive even when 1 is the default
    if (Number.isNaN(this.quantity)) {
      this.quantity = 0;
    }
  }

  /* endregion dependencies */

}
