import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-choose-mode',
  templateUrl: './choose-mode.component.html',
  styleUrls: ['./choose-mode.component.css']
})
export class ChooseModeComponent implements OnInit {
  _asTeacher: boolean = false;
  _asStudent: boolean = false;
  _asExperimenter : boolean = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  set asTeacher(value: boolean) {
    this._asTeacher = value;
    this._asStudent = !this._asTeacher;
  }

  get asTeacher() {
    return this._asTeacher;
  }

  set asStudent(value: boolean) {
    this._asStudent = value;
    this._asTeacher = !this._asStudent;
  }

  get asStudent() {
    return this._asStudent;
  }

  set asExperimenter(value: boolean) {
    this._asExperimenter = value;

    if (this._asExperimenter) {
      this._asStudent = false;
      this._asTeacher = false;
    }
  }

  get asExperimenter() {
    return this._asExperimenter;
  }

  clearMode() {
    this._asTeacher = false;
    this._asStudent = false;
    this._asExperimenter = false;
  }

}
