import {Component, OnInit} from '@angular/core';
import {PresenterBasis} from '../presenter/presenter-basis';
import { MaterialBasis } from "../material/material-basis";

/* A Supplier is a source of Presenters for a Definer. */

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {
  presenters: PresenterBasis[] = [];

  constructor() {
    // later, replace this with an integrated real solution
    for (let of = 1; of < 4; of++) {
      let material = new MaterialBasis(of - 1, `Material ${of}`);
      let basis = new PresenterBasis(of - 1, `Option ${of}`, material);
      this.presenters.push(basis);
    }
  }

  ngOnInit(): void {
  }

}
