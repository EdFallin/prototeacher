import {Component, OnInit, Input} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {ComposableComponent} from '../composable/composable.component';
import {DragAndDropService} from '../drag-and-drop/drag-and-drop.service';
import {MaterialBasis} from './material-basis';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent extends ComposableComponent {
  declaredType = "Material";

  @Input() basis: MaterialBasis;

  constructor(dnd: DragAndDropService) {
    super(dnd);
  }

  ngOnInit(): void {
  }

  get content(): any {
    return this.basis.content;
  }

  set content(value: any) {
    this.basis.content = value;
  }

}
