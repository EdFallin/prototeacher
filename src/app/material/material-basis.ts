/**/

import {ComposableBasis} from '../composable/composable-basis';

/* MaterialBasis is the stored definition of a Material,
   including references to any recursive structure. */

export class MaterialBasis extends ComposableBasis {
  content: any;

  constructor(id: number, name: string = null, content = null) {
    // constructor of superclass must be called, and should be called first
    super(id, name);

    // additional properties specific to this subclass
    this.content = content;

    // hack; later, replace with real type usage
    this.declaredType = "Material";
  }


  /* an override of ComposableBasis.clone() */
  clone() {
    let copy = new MaterialBasis(this.id, this.name, this.content);
    return copy;
  }
}
