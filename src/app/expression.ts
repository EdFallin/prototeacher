/**/

import {Output, EventEmitter, Component} from '@angular/core';

import {Lexeme} from './lexeme';
import {Operation} from './lexemes/operation';
import {EmptyOperation} from './lexemes/operations/empty-operation';
import {Operand} from './lexemes/operand';
import {EmptyOperand} from './lexemes/operands/empty-operand';
import {Notation} from './lexemes/notation';

/* later, should drop this if possible, getting it from a service instead */
import {Infix} from './lexemes/notations/infix';

/* decorated with this as though a component, but not really a component;
   later, find some way to use events and so on without @Component() */
export class Expression {
  /* defaults needed for template usage */
  notation: Notation = new Infix();  // default notation style
  lexemes: Lexeme[] = [new EmptyOperand(), new EmptyOperation(), new EmptyOperand()];

  constructor() {
    /* no operations */
  }

  /* clones of lexemes added are retained in matching class members / locations,
     replacing existing ones; cloned for identity-indexing by caller for at arg */
  addLexemeClone(lexeme: Lexeme, at: number) {
    /* an Operand overwrites the one at the same index;
       indices don't need to be correlated here,
       because they are found by ID upstream */
    if (lexeme instanceof Operand) {
      let to = at;

      // an Operand dropped on the Operation is ignored and tossed
      if (this.lexemes[at] instanceof Operation) {
        return;
      }

      // if not cloned, potentially the same identity at
      // multiple indices when calculating at in caller
      // let clone = lexeme.clone();
      this.lexemes[at] = lexeme;
    }

    /* wherever dropped, an Operation overwrites the existing one */
    if (lexeme instanceof Operation) {
      let to = this.lexemes
        .findIndex(x => x instanceof Operation);
      this.lexemes[to] = lexeme;
    }

    /* a new Notation simply overwrites the existing one */
    if (lexeme instanceof Notation) {
      this.notation = lexeme;
    }
  }

  /* the lexemes are ordered according to the notation's .correlations */
  get ordering(): Lexeme[] {
    // input and output Lexeme arrays
    let stencil = this.notation.correlations;
    let order = Array<Lexeme>(stencil.size);

    // traversing input array, to match what's present, in order
    for (let from of stencil.keys()) {
      let to = stencil.get(from);
      order[to] = this.lexemes[from];
    }

    // to caller
    return order;
  }

  /* each lexeme type has its own classes
     for a distinctive CSS styling */
  classByType(lexeme: Lexeme): string {
    if (lexeme instanceof EmptyOperation) {
      return 'chosen empty-operation';
    }

    if (lexeme instanceof EmptyOperand) {
      return 'chosen empty-operand';
    }

    if (lexeme instanceof Operation) {
      return 'chosen operation';
    }
    if (lexeme instanceof Operand) {
      return 'chosen operand';
    }

    return '';
  }

  /* this allows copying an Expression so multiple
     versions of it may be individually manipulated */
  clone() {
    /* a new Expression is returned, but all members of an Expression are much
       like Flyweights, so they don't need to be individually copied / cloned */

    let clone = new Expression();

    // simple replacement
    clone.notation = this.notation;

    // later, find a more succinct way, possibly
    // default .lexemes must be removed and replaced
    clone.lexemes = [];

    for (let lexeme of this.lexemes) {
      clone.lexemes.push(lexeme);
    }

    // to caller
    return clone;
  }

  /* later, this can be used to get the result of calculating
     the expression, probably using the Operation's .operate() */
  get calculate(): number {
    return -1;
  }
}
