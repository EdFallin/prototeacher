/**/

import {Expression} from './expression';
import {Notation} from './lexemes/notation';
import {EmptyOperand} from './lexemes/operands/empty-operand';
import {Multiplication} from "./lexemes/operations/multiplication";

describe('Expression', () => {
  it('should create an instance', () => {
    expect(new Expression()).toBeTruthy();
  });

  it('addLexemeClone() should replace operand with operand arg', () => {
    let expression = new Expression();

    let notation = new Notation();

    let lexeme = new EmptyOperand();
    lexeme.name = "arg";
    lexeme.id = "arg";

    let existing = expression.lexemes[0];
    let at = 0;

    expression.addLexemeClone(lexeme, at);

    expect(expression.lexemes[0]).toEqual(lexeme);
    expect(expression.lexemes[0]).not.toEqual(existing);
  });

  it("addLexemeClone() should not replace operator with operand arg", () => {
    let expression = new Expression();
    let notation = new Notation();

    // this correlation puts the operation in the middle, its default location
    notation.correlations = new Map([[0,0], [1,1], [2,2]]);
    expression.notation = notation;

    let lexeme = new EmptyOperand();
    lexeme.name = "operand";
    lexeme.id = "operand";

    // this is the index of the operation, rather than an operand
    let at = 1;

    let existing = expression.lexemes[at];

    expression.addLexemeClone(lexeme, at);

    expect(expression.lexemes[at]).not.toEqual(lexeme);
    expect(expression.lexemes[at]).toEqual(existing);
  });
});
