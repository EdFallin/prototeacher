/**/

import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Lexeme} from '../lexeme';

/* FrameComponent provides a drag-and-drop -ready frame for its Lexeme,
   allowing instance-identity handling instead of ID handling for drops */

@Component({
  selector: 'app-frame',
  templateUrl: './frame.component.html',
  styleUrls: ['./frame.component.css']
})
export class FrameComponent implements OnInit {
  @Input() at: number;
  @Input() lexeme: Lexeme;

  // this event is consumed by whatever component wants to get
  // the framed Lexeme when this Frame instance is dropped onto
  @Output() droppedOnto: EventEmitter<number>;

  @Input() styling: string;

  constructor() {
    this.droppedOnto = new EventEmitter<number>();
  }

  ngOnInit(): void {
  }

  whenDraggedOver(e: DragEvent) {
    // Javascript calls
    e.preventDefault();
    e.stopPropagation();
    e.dataTransfer.dropEffect = 'copy';
  }

  whenDropped(e: DragEvent) {
    // JavaScript calls still needed
    e.preventDefault();
    e.stopPropagation();

    // Angular event propagation,
    // bypassing .dataTransfer
    this.droppedOnto.emit(this.at);
  }
}
