import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ChooseModeComponent } from './choose-mode/choose-mode.component';
import { TeacherModeComponent } from './teacher-mode/teacher-mode.component';
import { StudentModeComponent } from './student-mode/student-mode.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { DragAndDropService } from "./drag-and-drop/drag-and-drop.service";
import { ComposableComponent } from './composable/composable.component';
import { PresenterComponent } from './presenter/presenter.component';
import { MaterialComponent } from './material/material.component';
import { DefinerComponent } from './definer/definer.component';
import { ExperimenterComponent } from './experimenter/experimenter.component';
import { SupplierComponent } from './supplier/supplier.component';
import { FrameComponent } from './frame/frame.component';

@NgModule({
  declarations: [
    AppComponent,
    ChooseModeComponent,
    TeacherModeComponent,
    StudentModeComponent,
    SpinnerComponent,
    ComposableComponent,
    PresenterComponent,
    MaterialComponent,
    DefinerComponent,
    ExperimenterComponent,
    SupplierComponent,
    FrameComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [DragAndDropService],
  bootstrap: [AppComponent]
})
export class AppModule { }
