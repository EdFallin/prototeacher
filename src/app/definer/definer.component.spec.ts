import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinerComponent } from './definer.component';

describe('DefinerComponent', () => {
  let component: DefinerComponent;
  let fixture: ComponentFixture<DefinerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefinerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
