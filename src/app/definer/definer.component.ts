import {Component, OnInit} from '@angular/core';
import {ComposableComponent} from '../composable/composable.component';
import {DragAndDropService} from '../drag-and-drop/drag-and-drop.service';
import {MaterialBasis} from '../material/material-basis';
import {PresenterBasis} from '../presenter/presenter-basis';

@Component({
  selector: 'app-definer',
  templateUrl: './definer.component.html',
  styleUrls: ['./definer.component.css']
})
export class DefinerComponent extends ComposableComponent {
  presenters: PresenterBasis[] = [];

  constructor(dnd: DragAndDropService) {
    super(dnd);
    this.droppableTypes.push("Presenter");
  }

  ngOnInit(): void {
  }

  dropHandler() : void {
    let draggees = this.dnd.getDraggees();
    let draggee = draggees[0];

    let copy = draggee.clone();
    this.presenters.push(<PresenterBasis>copy);
  }
}
