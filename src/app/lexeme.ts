/**/

export interface Lexeme {
  id: string;
  name: string;
}
