/**/

import { ComposableBasis } from "../composable/composable-basis";
import { MaterialBasis } from "../material/material-basis";

/* PresenterBasis is the stored definition of a Presenter,
   including any references in a recursive structure. */

export class PresenterBasis extends ComposableBasis {
  // recursive structure: this matches this instance's child presenters
  // and its material's child materials by local index (not storage ID)
  correlations = new Map();

  /* later, add retrieval of Material / construction
     of presenter-material matches based on IDs */

  // ID of a material that this instance presents
  materialId: number;

  // material for this presenter only; for now at least,
  // child presenters link to their own materials,
  // but that probably isn't the long-term solution
  material: MaterialBasis;

  constructor(id: number, name: string, material: MaterialBasis = null) {
    // constructor of superclass must be called, and should be called first
    super(id, name);

    // additional properties specific to this subclass
    this.material = material;

    // hack; later, replace with real type usage
    this.declaredType = "Presenter";
  }

  /* an override of ComposableBasis.clone() */
  clone() {
    let copy = new PresenterBasis(this.id, this.name, this.material);
    copy.correlations = new Map(this.correlations);
    return copy;
  }
}

