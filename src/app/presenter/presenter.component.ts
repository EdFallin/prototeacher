import {Component, OnInit, Input} from '@angular/core';
import {ComposableComponent} from "../composable/composable.component";
import {DragAndDropService} from "../drag-and-drop/drag-and-drop.service";
import {PresenterBasis} from './presenter-basis';
import { MaterialBasis } from "../material/material-basis";

@Component({
  selector: 'app-presenter',
  templateUrl: './presenter.component.html',
  styleUrls: ['./presenter.component.css']
})
export class PresenterComponent extends ComposableComponent {
  @Input() basis: PresenterBasis;
  @Input() asThumb: Boolean;

  constructor(dnd: DragAndDropService) {
    super(dnd);

    this.droppableTypes.push("Material");
  }

  get thumbName(): string {
    return this.basis.name;
  }

  set thumbName(value: string) {
    this.basis.name = value;
  }

  ngOnInit(): void {
  }

  dropHandler() : void {
  }
}
